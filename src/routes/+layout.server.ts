import type { LayoutServerLoad } from './$types'

export const load: LayoutServerLoad = async ({ locals }) => {

    /*
     * Deconstruct locals
     */
    const { theme } = locals;

    /*
     * Serve the data to +layout.svelte in /
     */
    return {
        theme,
    }

}
