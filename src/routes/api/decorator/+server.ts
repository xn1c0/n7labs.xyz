import { ten_yis } from "$home/lib/utils/appsettings";
import { isThemeValid } from "$home/lib/utils/theme_utils";
import { redirect, type RequestHandler } from "@sveltejs/kit"

/*
 * Prevent fetching from this route
 */
export const GET: RequestHandler = async () => {
    throw redirect(303, '/')
}

export const POST: RequestHandler = async ({ url, cookies }) => {

    /*
     * Get data sent by client
     */
    const data: URLSearchParams = url.searchParams;
    const theme: string = data.get('theme') ?? '';

    /*
     * Guard!
     * Data validation
     */
    if (!isThemeValid(theme)) {
        return fail(400);
    }

    /*
     * Set cookie theme
     */
    cookies.set('theme', theme, { path: '/', maxAge: ten_yis })

    return new Response(JSON.stringify({ success: true }));

};
