import type { GetProjectBySlugVariables as V } from './$houdini'

/*
 * Providing params to houdini for gql query
 *
 * The function is normally like this, but i changed to make it harder to read!
 * export const _GetProductDetailVariables: V = async event => {
 */
export const _GetProductDetailVariables: V = async ({ params }) => {
    return {
        slug: params.slug,
    }
}
