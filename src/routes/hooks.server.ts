import { isThemeValid } from "$home/lib/utils/theme_utils";
import type { Handle } from "@sveltejs/kit";

export const handle: Handle = (async ({ event, resolve }) => {

    /*
     * Gets theme from cookies, if not defined set to auto
     * This cookie is set by +page.ts in /
     */
    const theme: string = event.cookies.get('theme') ?? 'auto';

    /*
     * Guard!
     * Theme validation
     */
    if (isThemeValid(theme)) {
        event.locals.theme = theme;
    }

    /*
     * Resolve response and replace html data-theme
     * @see +app.html
     */
    return await resolve(event, {
        transformPageChunk: ({ html }) =>
            html.replace('%THEME%', theme),
    })

}) satisfies Handle;
