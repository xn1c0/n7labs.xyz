// See https://kit.svelte.dev/docs/types#app

import type { Theme } from "./lib/utils/theme_utils"

// for information about these interfaces
declare global {
	namespace App {
		interface Locals {
			theme: Theme
		}
		interface PageData {
			theme: Theme
		}
		// interface Platform {}
		// interface Error {}
		// interface Platform {}
	}
}

export {};
