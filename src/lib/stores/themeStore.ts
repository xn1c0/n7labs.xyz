import { writable } from "svelte/store";
import type { Theme } from "$lib/utils/theme_utils";

export const themeStore = writable<Theme>();
