interface Route {
    id: string,
    name: string
}

export default class NavRoute {
    static readonly home: Route = {id: '/', name: 'Home'};
    static readonly about: Route = {id: '/about', name: 'About'};
    static readonly projects: Route = {id: '/projects', name: 'Projects'};
    static readonly blog: Route = {id: '/blog', name: 'Blog'};
}
