/*
 * Generate a union type from an array.
 * https://stackoverflow.com/questions/51521808/how-do-i-create-a-type-from-a-string-array-in-typescript
 */

import { themes } from "./appsettings";

export type Theme = typeof themes[number];

export const isThemeValid = (theme: any): theme is Theme => {
    return Boolean(theme) && themes.includes(theme);
}
