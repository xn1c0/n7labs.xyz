export const themes: readonly string[] = [
    'auto', 'light', 'dark', 'cupcake', 'bumblebee', 'emerald', 'corporate',
    'synthwave', 'retro', 'cyberpunk', 'valentine', 'halloween', 'garden',
    'forest', 'aqua', 'lofi', 'pastel', 'fantasy', 'wireframe', 'black',
    'luxury', 'dracula', 'cmyk', 'autumn', 'business', 'acid', 'lemonade',
    'night', 'coffee', 'winter',
] as const;

export const five_mis: number = 5 * 60;
export const ten_yis: number = 10 * 365 * 24 * 60 * 60;
