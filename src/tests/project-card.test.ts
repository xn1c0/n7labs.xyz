import ProjectCard from '$lib/components/project-card.svelte';
import { vi } from 'vitest';
import { render, fireEvent } from '@testing-library/svelte'

describe("ProjectCard Test", () => {

    const project1 = {
        project_name: "name1",
        project_description: "description1",
        project_image_url: "image1",
        project_slug: "slug1"
    };

    const btnText: string = "Take me to it!";

    test("render project-card", () => {
        const { getByText } = render(ProjectCard, { props: { ...project1 } });
        expect(getByText("name1")).toBeInTheDocument();
        expect(getByText("description1", { exact: false })).toBeInTheDocument();
    });

    test("show image", () => {
        const { getByAltText } = render(ProjectCard, { props: { ...project1 } });
        const image = getByAltText("name1");
        expect(image).toHaveAttribute('src', 'image1')
    });

    test("dispatchNavToProject event", async () => {
        const { getByText, component } = render(ProjectCard, { props: { ...project1 } });
        const navToProjectBtn = getByText(btnText);
        let mockEvent = vi.fn();
        component.$on('navToProjectBtnClick', function (event) {
            mockEvent(event.detail);
        });
        await fireEvent.click(navToProjectBtn);
        expect(mockEvent).toHaveBeenCalled(); // to check if it's been called
        expect(mockEvent).toHaveBeenCalledWith({slug: "slug1"}); // to check the content of the event
    });
});
