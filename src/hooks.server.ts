import type { Handle } from "@sveltejs/kit";
import { isThemeValid } from "$lib/utils/theme_utils";

export const handle: Handle = async ({event, resolve}) => {

    console.log("HOOK");

    const theme: FormDataEntryValue = event.cookies.get('theme') ?? 'auto';

    if (isThemeValid(theme)) {
        event.locals.theme = theme;
    }

    return await resolve(event, {
        transformPageChunk: ({ html }) =>
            html.replace('%THEME%', theme),
    })

}
