/// <references types="houdini-svelte">

/** @type {import('houdini').ConfigFile} */
const config = {
    "plugins": {
        "houdini-svelte": {}
    },
    scalars: {
        DateTime: {
            type: 'Date',
            unmarshal(val) {
                return new Date(val)
            },
            marshal(date) {
                return date.getTime()
            }
        }
    }
}

export default config
