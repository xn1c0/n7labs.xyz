import path from 'path'
import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vitest/config';
import houdini from 'houdini/vite';

export default defineConfig({
	plugins: [houdini(), sveltekit()],
	resolve: {
		alias: {
			$home: path.resolve('./src'),
			$lib: path.resolve('./src/lib'),
		}
	},
	test: {
		// Jest like globals
		globals: true,
		environment: 'jsdom',
		include: ['src/**/*.{test,spec}.{js,ts}'],
		// Extend jest-dom matchers
		setupFiles: [path.resolve('./vitest.setup.ts')]
	}
});
